import React from 'react';
import express from 'express';
import {renderToStaticMarkup} from 'react-dom/server';
import robots from 'express-robots-txt';
import MySql from 'sync-mysql';
import fs from 'fs';

const expressSitemapXml = require('express-sitemap-xml');

const app = express();

const vers = `11.05.2024.1`; //versioning for js and css

// Set variables
const dev = process.env.NODE_ENV !== 'production';

const PORT = 8888;

const googleTag = `window.dataLayer = window.dataLayer || [];
           function gtag(){dataLayer.push(arguments);}
           gtag('js', new Date());

           gtag('config', 'UA-42455029-2');`;

app.use(robots(__dirname + '/robots.txt'));
app.use('/sw.js', (req, res) => {
  res.setHeader('Content-Type', 'application/javascript');
  res.writeHead(200);
  res.end(fs.readFileSync(`${__dirname}/sw.js`,
    {encoding:'utf8', flag:'r'}));
});

app.use(expressSitemapXml(
  () => {

    const baseUrls = [{
      url: '/',
      lastMod: new Date('2020-02-18'),
      changeFreq: 'weekly',
    },
      {
        url: '/photos',
        lastMod: new Date('2020-02-18'),
        changeFreq: 'weekly',
      },
      {
        url: '/about',
        lastMod: new Date('2020-02-18'),
        changeFreq: 'monthly',
      }];

    const connection = new MySql({
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      database: process.env.MYSQL_DB,
      password: process.env.MYSQL_PASS,
    });

    const data = connection.query(
      "SELECT * FROM `images` WHERE `visible` = 1"
    );

    connection.dispose();

    const photoUrls = data.map((dataItem) => ({
      url: `/photos/${dataItem.gid}/${dataItem.id}/`,
      lastMod: new Date(dataItem.created),
      changeFreq: 'never',
    }));

    return [ ...baseUrls, ...photoUrls ]
  }, 'https://aivis.lv'));

app.use('/lv/foto/album/',
  (req, res) => {
    const urlProps = req.url.split('/');

    return res.redirect(301, `https://aivis.lv/photos/${urlProps[1]}/${urlProps[3] || ''}/${urlProps[4] || ''}`);
});

const fbOGTags = async (req, res) => {

  const urlProps = req.url.split('/');

  let share = null;

  const connection = new MySql({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    database: process.env.MYSQL_DB,
    password: process.env.MYSQL_PASS,
  });

  if (urlProps.length >= 3) {
    const data = connection.query(
      `SELECT * FROM \`images\` WHERE \`id\` = ${Number(urlProps[2])}`
    );

    if (data && data[0]) {

      let description = '';

      if (data[0].title !== '') {
        description = `Title: ${data[0].title}`;
      }

      if (data[0].place !== '') {
        if (description !== '') {
          description = `${description}; Location: ${data[0].place}`
        } else {
          description = data[0].place;
        }
      }

      if (data[0].persons !== '') {
        if (description !== '') {
          description = `${description}; Persons: ${data[0].persons}`
        } else {
          description = data[0].persons;
        }
      }

      share = {
        title: data[0].title,
        id: data[0].id,
        gid: data[0].gid,
        description,
      }
    }

  } else if (urlProps.length == 2) {
    const data = connection.query(
      `SELECT * FROM \`albums\` WHERE \`id\` = ${Number(urlProps[1])}`
    );

    if (data && data[0]) {
      const image = connection.query(
        `SELECT * FROM \`images\` WHERE \`id\` = ${data[0].image}`
      );

      let description = '';

      if (image[0].title !== '') {
        description = `Title: ${image[0].title}`;
      }

      if (image[0].place !== '') {
        if (description !== '') {
          description = `${description}; Location: ${image[0].place}`
        } else {
          description = image[0].place;
        }
      }

      if (image[0].persons !== '') {
        if (description !== '') {
          description = `${description}; Persons: ${image[0].persons}`
        } else {
          description = image[0].persons;
        }
      }

      share = {
        title: `Album - ${data[0].title}`,
        id: data[0].image,
        gid: data[0].id,
        description,
      }
    }
  }

  connection.dispose();

  res.send(
    share ? withShareMeta(share) : withoutShareMeta()
  );
}

app.use('/photos/', fbOGTags);
app.use('/sessions/', fbOGTags);

app.use(
  '/',
  (req, res) => {
    res.send(
      withoutShareMeta()
    );
  }
);

app.listen(PORT);

function withShareMeta(meta) {
  return '<!DOCTYPE html>\n' + renderToStaticMarkup(
    <html lang="en">
    <head>
      <meta httpEquiv="content-language" content="en" />
      <meta content="initial-scale=1, maximum-scale=1 shrink-to-fit=no, user-scalable=no" name="viewport" />
      <meta content="ie=edge" httpEquiv="x-ua-compatible" />
      <meta property="fb:app_id" content="483133628493176" />
      <meta property="og:url" content={`https://aivis.lv/photos/${meta.gid}/${meta.id}`} />
      <meta property="og:type" content="article" />
      <meta property="og:title" content={meta.title} />
      <meta property="og:description" content={meta.description} />
      <meta property="og:image" content={`https://cdn.aivis.lv/gallery/1920/${meta.id}.jpg`} />
      <meta name="google-site-verification" content="jlBYVH4TPm55-Vt1I03KbfOx_9hgaYLlUf5brdHQpkQ" />
      <title>aivis.lv</title>
      <link href={process.env.STATIC_ROOT} rel='preconnect' crossOrigin="true" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/vendor.bundle.js?v=${vers}`} as="script" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/app.bundle.js?v=${vers}`} as="script" />
      <link rel="preload" crossOrigin="true" href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,600,700,800&subset=latin,latin-ext&display=swap" as="style" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/app.css?v=${vers}`} as="style" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/carousel.css`} as="style" />

      <link rel="stylesheet" href={`${process.env.STATIC_ROOT}/app.css?v=${vers}`} />
      <link rel="stylesheet" href={`${process.env.STATIC_ROOT}/carousel.css`} />
      <link rel="stylesheet" crossOrigin="true" href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,600,700,800&subset=latin,latin-ext" />

      <link rel="shortcut icon" crossOrigin="true" href={`${process.env.STATIC_ROOT}/images/favicon.png`} />
      <meta property="og:site_name" content="aivis.lv" />
      <meta property="fb:admins" content="1312451045" />
      <script dangerouslySetInnerHTML={{ __html: `window.setEnvironment = '${process.env.NODE_ENV}';` }} />
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42455029-2" />
      <script dangerouslySetInnerHTML={{ __html: googleTag }} />
    </head>
    <body>
    <div id="content" />
    <script src={`${process.env.STATIC_ROOT}/vendor.bundle.js?v=${vers}`} />
    <script src={`${process.env.STATIC_ROOT}/app.bundle.js?v=${vers}`} />
    <script dangerouslySetInnerHTML={{ __html: sw }}></script>
    </body>
    </html>
  )
}

function withoutShareMeta() {
  return '<!DOCTYPE html>\n' + renderToStaticMarkup(
    <html lang="en">
    <head>
      <meta httpEquiv="content-language" content="en" />
      <meta content="initial-scale=1, maximum-scale=1 shrink-to-fit=no, user-scalable=no" name="viewport" />
      <meta content="ie=edge" httpEquiv="x-ua-compatible" />
      <title>aivis.lv</title>
      <link href={process.env.STATIC_ROOT} rel='preconnect' crossOrigin="true" />
      <meta property="fb:app_id" content="483133628493176" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/vendor.bundle.js?v=${vers}`} as="script" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/app.bundle.js?v=${vers}`} as="script" />
      <link rel="preload" crossOrigin="true" href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,600,700,800&subset=latin,latin-ext" as="style" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/app.css?v=${vers}`} as="style" />
      <meta name="google-site-verification" content="jlBYVH4TPm55-Vt1I03KbfOx_9hgaYLlUf5brdHQpkQ" />
      <link rel="preload" href={`${process.env.STATIC_ROOT}/carousel.css`} as="style" />

      <link rel="stylesheet" href={`${process.env.STATIC_ROOT}/app.css?v=${vers}`} />
      <link rel="stylesheet" href={`${process.env.STATIC_ROOT}/carousel.css`} />
      <link rel="stylesheet" crossOrigin="true" href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,600,700,800&subset=latin,latin-ext" />

      <link rel="shortcut icon" crossOrigin="true" href={`${process.env.STATIC_ROOT}/images/favicon.png`} />
      <meta property="og:site_name" content="aivis.lv" />
      <meta property="fb:admins" content="1312451045" />
      <script dangerouslySetInnerHTML={{ __html: `window.setEnvironment = '${process.env.NODE_ENV}';` }} />
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42455029-2" />
      <script dangerouslySetInnerHTML={{ __html: googleTag }} />
    </head>
    <body>
    <div id="content" />
    <script src={`${process.env.STATIC_ROOT}/vendor.bundle.js?v=${vers}`} />
    <script src={`${process.env.STATIC_ROOT}/app.bundle.js?v=${vers}`} />
    <script dangerouslySetInnerHTML={{ __html: sw }}></script>

    </body>
    </html>
  )
}

const sw = ``;
