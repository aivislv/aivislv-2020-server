# Group 1
User-agent: Googlebot
Disallow: /games/
Disallow: /anime/
Disallow: /movies/

# Group 2
User-agent: *
Allow: /

Sitemap: http://aivis.lv/sitemap.xml