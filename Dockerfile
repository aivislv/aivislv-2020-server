FROM node:13.3.0

WORKDIR /usr/src/app/

ARG NODE=production
ENV NODE_ENV ${NODE}

CMD ["npm", "start"]

EXPOSE 8888